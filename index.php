<?php
    session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>home</title>
	</head>
	<body>
		<?php
		    require_once('util.php');
		    if(!check_authenticated()) {
			    echo "<a href=\"login.php\">Log in</a>";
	        } else {
		        echo "<a href=\"logout.php\">Log out</a> | " . $_SESSION['username'];
            }
		?>

		<h1>Add Post</h1>
		<?php
			if(check_authenticated()) {
				echo "<form method=\"POST\" action=\"newPost.php\">";
				echo	"<textarea name=\"contents\"></textarea><br />";
				echo 	"<input type=\"submit\" value=\"New Post\">";
				echo "</form>";
			}
			else
			{
				echo "Must Sign in to post.";
			}
		 ?>


		<h1>Blog Posts</h1>
		<hr />
		<?php
			require_once('mysqlconnect.php');
			$result = $conn->query("SELECT * from posts");
			while($row = $result->fetch_assoc()) {
                // sanatization
                $post = htmlspecialchars($row["content"]);
                $user = htmlspecialchars($row["user_id"]);

				echo "<p>" . $post . "</p>";
				echo "<p>Posted by " . $user . "</p><hr />";
			}
	    ?>
	</body>
</html>

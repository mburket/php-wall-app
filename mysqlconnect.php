<?php
/**
 * Created by PhpStorm.
 * User: mburket
 * Date: 3/1/16
 * Time: 12:00 PM
 */

$username = "root";
$password = "";
$host = "localhost";
$database = "php_wall_app";

$conn = new mysqli($host, $username, $password, $database);

if($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
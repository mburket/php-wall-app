<?php
    require_once('util.php');
    require_once('mysqlconnect.php');

    require_authenticated();
    $postContent = $_POST['contents'];
    $stmt = $conn->prepare("INSERT INTO posts(user_id, content, posted_at) VALUES (?, ?, current_date());");

    if ($stmt) {
        $stmt->bind_param("ss", $_SESSION['username'], $postContent);
        $stmt->execute();
    }

    mysqli_close($conn);
    unset($conn);
    header("Location: index.php");
?>

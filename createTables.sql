CREATE TABLE `php_wall_app`.`posts` (
  `post_id` INT NOT NULL AUTO_INCREMENT,
  `user_id` varchar(45) NULL,
  `content` VARCHAR(800) NULL,
  `posted_at` DATETIME NULL,
  PRIMARY KEY (`post_id`),
  INDEX `fk_posts_1_idx` (`user_id` ASC));

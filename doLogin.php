<?php
    // sanitization
    $ldap = ldap_connect("ldap.jmay.us");
    $user = ldap_escape($_POST['username'], NULL, LDAP_ESCAPE_FILTER);

    // Do not allow empty passwords or usernames
    if ($_POST['password'] && $user) {
        $authenticated = ldap_bind($ldap, "CN=" . $user . ",CN=phpwall,DC=team1,DC=isucdc,DC=com", $_POST['password']);
    } else {
        $authenticated = FALSE;
    }

    if ($authenticated) {
        session_start();
        $_SESSION['username'] = $user;
        $_SESSION['auth_id'] = hash("sha256", openssl_random_pseudo_bytes(200));
        $_SESSION['start_time'] = time();
        $_SESSION['last_request'] = time();
        $_SESSION['logged_in'] = true;
        $_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
        $_SESSION['remote_ip'] = $_SERVER['REMOTE_ADDR'];
        header("Location: index.php");
    } else {
        echo("Login failed.  Please <a href='login.php'>try again</a>.");
    }
?>

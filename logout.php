<?php
    require_once('util.php');

    destroy_session();
    header("Location: index.php");
?>
